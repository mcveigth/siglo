#include "array_utils.h"
#include <stdio.h>

/**
 * Prints array
 * @param array
 * @param size
*/
void print_array(int* array, int size) {
    printf("[");
    for(int i = 0; i < size; i++) {
        printf("%d", array[i]);
        if (i < size - 1) {
            printf(", ");
        }
    }
    printf("]\n");
}

void print_array2D(int** array, int rows, int cols) {
    printf("[");
    for (int i = 0; i < rows; i++) {
        printf("[");
        for (int j = 0; j < cols; j++) {
            printf("%d", array[i][j]);
            if (j < cols - 1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < rows - 1) {
            printf(",\n");
        }
    }
    printf("]\n");
}