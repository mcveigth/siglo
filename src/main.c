#include <stdio.h>
#include <stdlib.h>
#include "bolas.h"
#include "player.h"
#include "array_utils.h"

int main(void) {
   int number_of_players = get_number_of_players();
   int* bolas = create_bolas();
   shuffle_bolas(bolas);

   int** players = create_players(number_of_players);

   print_array(bolas, BOLAS_SIZE);

   // for (int i = 0; i < number_of_players; i++) {
   //    for (int j = 0; j < DEFAULT_BOLA_SIZE; j++) {
   //       players[i][j] = bolas[j];
   //    }
   // }
   int i, j, k;
   for (i = 0, j = 0; i < number_of_players; i++){
      for(k = 0; k < DEFAULT_BOLA_SIZE; k++) {
         players[i][k] = bolas[j++];
         if (j >= DEFAULT_BOLA_SIZE) {
            j = 0;
         }
      }
   }
   print_array2D(players, number_of_players, DEFAULT_BOLA_SIZE);
   
   return 0; 
}