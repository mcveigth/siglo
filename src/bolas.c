#include "bolas.h"
#include <stdlib.h>

/**
 * generates an array with values from 1 to 9
 * @return array with ints from 1 to 9
*/
int* create_bolas(void) {
    int* bolas = (int*) malloc(BOLAS_SIZE * sizeof(int));
    for(int i = 0; i < BOLAS_SIZE; i++)
        bolas[i] = i + 1;
    
    return bolas;
}

/**
 * shuffles an array(bolas)
 * @param bolas
*/
void shuffle_bolas(int* const bolas) {
    for (int i = 0; i < BOLAS_SIZE; i++) {
        int j = rand() % BOLAS_SIZE;
        int tmp = bolas[j];

        bolas[j] = bolas[i];
        bolas[i] = tmp;
    }
}

/**
 * Sums player bolas
*/
int sum_bolas(int* player_bolas, int size) {
    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum += player_bolas[i];
    }
    return sum;
}