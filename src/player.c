#include "player.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * gets number of players from user
 * @return number of players
*/
int get_number_of_players() {
    int players;
    while(1) {
        printf("Enter amount of players: ");
        scanf("%d", &players);
        if (players <= 1) {
            printf("Number of players should be greater than 1\n");
        }
        else if (players > 7) {
            printf("Number of players should be less than 7\n");
        }
        else {
            return players;
        }
    }
}

/**
 * Creates 2D array to store players
 * @param number of players
 * @return 2D array with each player
*/
int** create_players(int number_players) {
    // allocate memory for each player (outer array)
    int** players = (int**) malloc(number_players * sizeof(int *));
    
    // allocate memory for each player's bola (inner array)
    for (int i = 0; i < number_players; i++) {
        players[i] = (int *)malloc(DEFAULT_BOLA_SIZE * sizeof(int));
    }
    return players;
}