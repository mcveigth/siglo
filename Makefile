CC = gcc
CFLAGS = -Iinclude

SRCS = $(wildcard src/*.c)
OBJS = $(patsubst src/%.c, build/%.o, $(SRCS))
HDRS = $(wildcard include/*.h)

bin/main: $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^

build/%.o: src/%.c $(HDRS)
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f build/*.o bin/*