#pragma once

/**
 * Balls array size
*/
#define BOLAS_SIZE 90
int* create_bolas(void);
void shuffle_bolas(int* const bolas);
int sum_bolas(int* player_bolas, int size);